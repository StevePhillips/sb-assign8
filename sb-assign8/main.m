#import "Driver.h"
#import "Driver2.h"

int main()
{
  Driver1 * driver = [[Driver1 alloc] init];

  [driver run];

  [[[Driver2 alloc]init]run];

  return 0;
}
