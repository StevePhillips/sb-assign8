//
//  RationalNumber.h
//  sb-assign8
//
//  Created by Student on 2014-10-29.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RationalNumber : NSObject

@property int numerator, denominator;

- (instancetype) initNumerator:(int)numer andDenom:(int)denom;

- (RationalNumber *) reciprocal;

- (RationalNumber *) add:(RationalNumber *)op2;
- (RationalNumber *) subtract:(RationalNumber *)op2;
- (RationalNumber *) multiply:(RationalNumber *)op2;
- (RationalNumber *) divide:(RationalNumber *)op2;

- (BOOL) isLike:(RationalNumber *)op2;

- (NSString *) description;

- (void) reduce;
- (int) gcd:(int)num1:(int)num2;
@end
