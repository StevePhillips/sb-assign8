#import "Driver.h"

@implementation Driver1

- (void) run
{
  const int max_stars = 10;

  // Triangle A
  for (int iter1 = max_stars; iter1 >= 0; iter1--)
  {
    for (int iter2 = iter1; iter2 >= 0; iter2--)
    {
      putchar('*');
    }
    putchar('\n');
  }
  // Triangle B
  // This is how many lines to output
  for (int iter1 = 1; iter1 <= max_stars; iter1++)
  {
    // Calculate how many spaces to output
    int nspaces = max_stars - iter1;

    for (int iter2 = nspaces; iter2 >= 0; iter2--)
    {
      putchar(' ');
    }

    for (int iter3 = 0; iter3 < iter1; iter3++)
    {
      putchar('*');
    }
    putchar('\n');
  }
  putchar('\n');
  // Triangle C
  for (int iter1 = max_stars; iter1 >= 0; iter1--)
  {
    // Calculate how many spaces to output
    int nspaces = max_stars - iter1;

    for (int iter2 = 0; iter2 < nspaces; iter2++)
    {
      putchar(' ');
    }

    for (int iter3 = 0; iter3 < iter1; iter3++)
    {
      putchar('*');
    }
    putchar('\n');
  }
  // Diamond
  for (int iter1 = 1; iter1 <= max_stars; iter1 += 2)
  {
    // should result in 4 for the first iteration, then three
    int nspaces = max_stars - iter1;

    for (int iter2 = 0; iter2 < nspaces / 2; iter2++)
    {
      putchar(' ');
    }

    for (int iter3 = 0; iter3 < iter1; iter3++)
    {
      putchar('*');
    }

    putchar('\n');
  }
  for (int iter1 = max_stars - 3; iter1 >= 0; iter1 -= 2)
  {
    // should result in 4 for the first iteration, then three
    int nspaces = max_stars - iter1;

    for (int iter2 = 0; iter2 < nspaces / 2; iter2++)
    {
      putchar(' ');
    }

    for (int iter3 = 0; iter3 < iter1; iter3++)
    {
      putchar('*');
    }

    putchar('\n');
  }
}
@end
