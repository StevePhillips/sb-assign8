//
//  RationalNumber.m
//  sb-assign8
//
//  Created by Student on 2014-10-29.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "RationalNumber.h"

@implementation RationalNumber

- (instancetype) initNumerator:(int)numer andDenom:(int)denom
{
  if (self = [super init])
  {
    if (denom == 0)
    {
      denom = 1;
    }

    if (denom < 0)
    {
      numer = numer * -1;
      denom = denom * -1;
    }

    _numerator = numer;
    _denominator = denom;

    [self reduce];

    return self;
  }
  return nil;
}

- (RationalNumber *) reciprocal
{
  return [[RationalNumber alloc]initNumerator:_denominator andDenom:_numerator];
}

- (RationalNumber *) add:(RationalNumber *)op2
{
  int commonDenominator = _denominator * op2.denominator;
  int numerator1 = _numerator * op2.denominator;
  int numerator2 = op2.numerator * _denominator;
  int sum = numerator1 + numerator2;

  return [[RationalNumber alloc] initNumerator:sum andDenom:commonDenominator];
}

- (RationalNumber *) subtract:(RationalNumber *)op2
{
  int commonDenominator = _denominator * op2.denominator;
  int numerator1 = _numerator * op2.denominator;
  int numerator2 = op2.numerator * _denominator;
  int difference = numerator1 - numerator2;

  return [[RationalNumber alloc] initNumerator:
          difference andDenom:commonDenominator];
}

- (RationalNumber *) multiply:(RationalNumber *)op2
{
  int numer = _numerator * op2.numerator;
  int denom = _denominator * op2.denominator;

  return [[RationalNumber alloc] initNumerator:numer andDenom:denom];
}

- (RationalNumber *) divide:(RationalNumber *)op2
{
  return [self multiply:[op2 reciprocal] ];
}

- (BOOL) isLike:(RationalNumber *)op2
{
  return (_numerator == op2.numerator && _denominator == op2.denominator);
}

- (NSString *) description
{
  return [NSString stringWithFormat:@"%i/%i", _numerator, _denominator];
}

- (void) reduce
{
  if (_numerator != 0)
  {
    int common = [self gcd:abs(_numerator):_denominator];

    _numerator = _numerator / common;
    _denominator = _denominator / common;
  }
}
- (int) gcd:(int)num1:(int)num2
{
  while (num1 != num2)
  {
    if (num1 > num2)
    {
      num1 -= num2;
    }
    else
    {
      num2 -= num1;
    }
  }

  return num1;
}

@end
