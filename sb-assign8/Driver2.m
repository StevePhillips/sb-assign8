//
//  Driver2.m
//  sb-assign8
//
//  Created by Student on 2014-10-29.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "Driver2.h"
#import "RationalNumber.h"

@implementation Driver2

- (void) run
{
  RationalNumber * frac1 = [[RationalNumber alloc] initNumerator:1 andDenom:2];
  RationalNumber * frac2 = [[RationalNumber alloc] initNumerator:1 andDenom:2];

  frac2 = [frac1 add:frac2];
  // Expect to see 1/1
  NSLog(@"%@", [frac2 description]);
  
  frac1.numerator = 3;
  frac1.denominator = 8;
  
  frac2.denominator = 5;
  
  frac1 = [frac2 divide:frac1];
  
  NSLog(@"%@", frac1);
  
  frac1.denominator++;
  [frac1 reduce];
  
  NSLog(@"%@", frac1);
  
  frac2.numerator = 6;
  
  frac1 = [frac2 multiply:frac1];
  
  NSLog(@"%@", frac1);
}
@end
